console.log('动态制作加载新的积木')
function ss_blockly_inject(Blockly) {
    console.log('开给 blockly 注入新积木属性')

//****************************************** function to forward the raspbot ******************************************/ 

    // Define the Style of designed Blocks
    Blockly.defineBlocksWithJsonArray([
        {
            type: "ss_raspbot_forward", 
            message0: "%{BKY_SS_RASPBOT_FORWARD}", 
            args0:[{
                type: "input_value",
                name: "LEFT",
                check: "Number"
            }, {
                type: "input_value",
                name: "RIGHT",
                check: "Number"
            }, {
                type: "input_value",
                name: "TIME",
                check: "Number"
            }],
            previousStatement: null,
            nextStatement: null,
            colour: "%{BKY_COLOR_OF_ACTION}" 
        }
    ]);

    // assign python code to corresponding blocks
    Blockly.Python['ss_raspbot_forward'] = function (block) {
        var argument0 = Blockly.Python.valueToCode(block, 'LEFT', Blockly.Python.ORDER_ATOMIC) || 'None';
        var argument1 = Blockly.Python.valueToCode(block, 'RIGHT', Blockly.Python.ORDER_ATOMIC) || 'None';
        var argument2 = Blockly.Python.valueToCode(block, 'TIME', Blockly.Python.ORDER_ATOMIC) || 'None';
        var code = Blockly.Python.format("forward({0}, {1}, {2})", argument0, argument1, argument2); 
        return code + "\n"
    };
    // displayed content under multiple languages
    Blockly.Msg.en["SS_RASPBOT_FORWARD"] = "forward %1, %2, %3 seconds";
    Blockly.Msg.zhhant["SS_RASPBOT_FORWARD"] = "前進 %1, %2, %3 秒";

//**************************************** */ function to backward the raspbot ******************************************/

    // Define the Style of designed Blocks
    Blockly.defineBlocksWithJsonArray([
        {
            type: "ss_raspbot_backward", 
            message0: "%{BKY_SS_RASPBOT_BACKWARD}", 
            args0:[{
                type: "input_value",
                name: "LEFT",
                check: "Number"
            }, {
                type: "input_value",
                name: "RIGHT",
                check: "Number"
            }, {
                type: "input_value",
                name: "TIME",
                check: "Number"
            }],
            previousStatement: null,
            nextStatement: null,
            colour: "%{BKY_COLOR_OF_ACTION}" 
        }
    ]);

    // assign python code to corresponding blocks
    Blockly.Python['ss_raspbot_backward'] = function (block) {
        var argument0 = Blockly.Python.valueToCode(block, 'LEFT', Blockly.Python.ORDER_ATOMIC) || 'None';
        var argument1 = Blockly.Python.valueToCode(block, 'RIGHT', Blockly.Python.ORDER_ATOMIC) || 'None';
        var argument2 = Blockly.Python.valueToCode(block, 'TIME', Blockly.Python.ORDER_ATOMIC) || 'None';
        var code = Blockly.Python.format("backward({0}, {1}, {2})", argument0, argument1, argument2); 
        return code + "\n"
    };
    // displayed content under multiple languages
    Blockly.Msg.en["SS_RASPBOT_BACKWARD"] = "backward %1, %2, %3 seconds";
    Blockly.Msg.zhhant["SS_RASPBOT_BACKWARD"] = "後退 %1, %2, %3 秒";

//******************************** */ Funtion to spin the raspbot left ******************************************/

     // Define the Style of designed Blocks
     Blockly.defineBlocksWithJsonArray([
        {
            type: "ss_raspbot_spin_left", 
            message0: "%{BKY_SS_RASPBOT_SPIN_LEFT}", 
            args0:[{
                type: "input_value",
                name: "LEFT",
                check: "Number"
            }, {
                type: "input_value",
                name: "RIGHT",
                check: "Number"
            }, {
                type: "input_value",
                name: "TIME",
                check: "Number"
            }],
            previousStatement: null,
            nextStatement: null,
            colour: "%{BKY_COLOR_OF_ACTION}" 
        }
    ]);

    // assign python code to corresponding blocks
    Blockly.Python['ss_raspbot_spin_left'] = function (block) {
        var argument0 = Blockly.Python.valueToCode(block, 'LEFT', Blockly.Python.ORDER_ATOMIC) || 'None';
        var argument1 = Blockly.Python.valueToCode(block, 'RIGHT', Blockly.Python.ORDER_ATOMIC) || 'None';
        var argument2 = Blockly.Python.valueToCode(block, 'TIME', Blockly.Python.ORDER_ATOMIC) || 'None';
        var code = Blockly.Python.format("spin_left({0}, {1}, {2})", argument0, argument1, argument2); 
        return code + "\n"
    };
    // displayed content under multiple languages
    Blockly.Msg.en["SS_RASPBOT_SPIN_LEFT"] = "spin left %1, %2, %3 seconds";
    Blockly.Msg.zhhant["SS_RASPBOT_SPIN_LEFT"] = "向左自轉 %1, %2, %3 秒";

//****************************** */ Funtion to spin the raspbot right ***************************************/

     // Define the Style of designed Blocks
     Blockly.defineBlocksWithJsonArray([
        {
            type: "ss_raspbot_spin_right", 
            message0: "%{BKY_SS_RASPBOT_SPIN_RIGHT}", 
            args0:[{
                type: "input_value",
                name: "LEFT",
                check: "Number"
            }, {
                type: "input_value",
                name: "RIGHT",
                check: "Number"
            }, {
                type: "input_value",
                name: "TIME",
                check: "Number"
            }],
            previousStatement: null,
            nextStatement: null,
            colour: "%{BKY_COLOR_OF_ACTION}" 
        }
    ]);

    // assign python code to corresponding blocks
    Blockly.Python['ss_raspbot_spin_right'] = function (block) {
        var argument0 = Blockly.Python.valueToCode(block, 'LEFT', Blockly.Python.ORDER_ATOMIC) || 'None';
        var argument1 = Blockly.Python.valueToCode(block, 'RIGHT', Blockly.Python.ORDER_ATOMIC) || 'None';
        var argument2 = Blockly.Python.valueToCode(block, 'TIME', Blockly.Python.ORDER_ATOMIC) || 'None';
        var code = Blockly.Python.format("spin_right({0}, {1}, {2})", argument0, argument1, argument2); 
        return code + "\n"
    };
    // displayed content under multiple languages
    Blockly.Msg.en["SS_RASPBOT_SPIN_RIGHT"] = "spin right %1, %2, %3 seconds";
    Blockly.Msg.zhhant["SS_RASPBOT_SPIN_RIGHT"] = "向右自轉 %1, %2, %3 秒";

//***************** */ function to rotate the upper and lower servo **************************************/

     // Define the Style of designed Blocks
     Blockly.defineBlocksWithJsonArray([
        {
            type: "ss_raspbot_rotate_servo", 
            message0: "%{BKY_SS_RASPBOT_ROTATE_SERVO}", 
            args0:[{
                type: "input_value",
                name: "UPPER",
                check: "Number"
            }, {
                type: "input_value",
                name: "LOWER",
                check: "Number"
            }],
            previousStatement: null,
            nextStatement: null,
            colour: "%{BKY_COLOR_OF_ACTION}" 
        }
    ]);

    // assign python code to corresponding blocks
    Blockly.Python['ss_raspbot_rotate_servo'] = function (block) {
        var argument0 = Blockly.Python.valueToCode(block, 'UPPER', Blockly.Python.ORDER_ATOMIC) || 'None';
        var argument1 = Blockly.Python.valueToCode(block, 'LOWER', Blockly.Python.ORDER_ATOMIC) || 'None';
        var code = Blockly.Python.format("servo({0}, {1})", argument0, argument1); 
        return code + "\n"
    };
    // displayed content under multiple languages
    Blockly.Msg.en["SS_RASPBOT_ROTATE_SERVO"] = "servo rotate %1, %2 degree";
    Blockly.Msg.zhhant["SS_RASPBOT_ROTATE_SERVO"] = "雲台旋轉 %1, %2 度";

//************************** */ function to detect the front distance with ultrasound sensor ************************************/

    Blockly.defineBlocksWithJsonArray([{
        type: "ss_raspbot_getdistance",
        message0: "%{BKY_SS_RASPBOT_GETDISTANCE}",
        output: "Boolean",
        outputShape: Blockly.OUTPUT_SHAPE_ROUND,
        colour: "%{BKY_COLOR_OF_DETECTION}"
    }]);

    /*** Blockly Python **/
    Blockly.Python['ss_raspbot_getdistance'] = function (block) {
        var order = Blockly.Python.ORDER_ATOMIC;
        var code = Blockly.Python.format("getDistance()");
        return [code, order];
    };

    /*** Blokly Message **/
    Blockly.Msg.en["SS_RASPBOT_GETDISTANCE"] = "distance";
    Blockly.Msg.zhhant["SS_RASPBOT_GETDISTANCE"] = "檢測到的距離";

//************************* */ function to beep ********************************************************************/

    Blockly.defineBlocksWithJsonArray([{
        type: "ss_raspbot_beep",
        message0: "%{BKY_SS_RASPBOT_BEEP}",
        previousStatement: null,
        nextStatement: null,
        colour: "%{BKY_COLOR_OF_ACTION}"
    }]);

    /*** Blockly Python **/
    Blockly.Python['ss_raspbot_beep'] = function (block) {
        var order = Blockly.Python.ORDER_ATOMIC;
        var code = Blockly.Python.format("beep()");
        return code + "\n";
    };

    /*** Blokly Message **/
    Blockly.Msg.en["SS_RASPBOT_BEEP"] = "beep";
    Blockly.Msg.zhhant["SS_RASPBOT_BEEP"] = "滴滴滴";

//***************** function to control the status of LED **************************************/

     // Define the Style of designed Blocks
     Blockly.defineBlocksWithJsonArray([
        {
            type: "ss_raspbot_led", 
            message0: "%{BKY_SS_RASPBOT_LED}", 
            args0:[{
                type: "field_dropdown",
                name: "RED_STATUS",
                options: [
                    ["%{BKY_SS_RASPBOT_BOOLEAN_HIGH}","True"],
                    ["%{BKY_SS_RASPBOT_BOOLEAN_LOW}","False"],
                ]
            }, {
                type: "field_dropdown",
                name: "BLUE_STATUS",
                options: [
                    ["%{BKY_SS_RASPBOT_BOOLEAN_HIGH}","True"],
                    ["%{BKY_SS_RASPBOT_BOOLEAN_LOW}","False"],
                ]
            }],
            previousStatement: null,
            nextStatement: null,
            colour: "%{BKY_COLOR_OF_ACTION}" 
        }
    ]);

    // assign python code to corresponding blocks
    Blockly.Python['ss_raspbot_led'] = function (block) {
        var argument0 = block.getFieldValue("RED_STATUS");
        var argument1 = block.getFieldValue("BLUE_STATUS");
        var code = Blockly.Python.format("led({0}, {1})", argument0, argument1); 
        return code + "\n"
    };
    // displayed content under multiple languages
    Blockly.Msg.en["SS_RASPBOT_LED"] = "set the red LED %1 and the blue LED %2";
    Blockly.Msg.zhhant["SS_RASPBOT_LED"] = "設置紅色LED %1 和藍色LED %2";

    Blockly.Msg.en["SS_RASPBOT_BOOLEAN_HIGH"] = "ON";
    Blockly.Msg.zhhant["SS_RASPBOT_BOOLEAN_HIGH"] = "開";

    Blockly.Msg.en["SS_RASPBOT_BOOLEAN_LOW"] = "OFF";
    Blockly.Msg.zhhant["SS_RASPBOT_BOOLEAN_LOW"] = "關";

//************************** */ function to detect the front distance with ultrasound sensor ************************************/

    Blockly.defineBlocksWithJsonArray([{
        type: "ss_raspbot_line_tracking",
        message0: "%{BKY_SS_RASPBOT_LINE_TRACKING}",
        output: "Boolean",
        outputShape: Blockly.OUTPUT_SHAPE_ROUND,
        colour: "%{BKY_COLOR_OF_DETECTION}"
    }]);

    /*** Blockly Python **/
    Blockly.Python['ss_raspbot_line_tracking'] = function (block) {
        var order = Blockly.Python.ORDER_ATOMIC;
        var code = Blockly.Python.format("tracking()");
        return [code, order];
    };

    /*** Blokly Message **/
    Blockly.Msg.en["SS_RASPBOT_LINE_TRACKING"] = "line tracking";
    Blockly.Msg.zhhant["SS_RASPBOT_LINE_TRACKING"] = "線跟踪";

//************************** */ function to detect the front distance with ultrasound sensor ************************************/

    Blockly.defineBlocksWithJsonArray([{
        type: "ss_raspbot_ir_avoid",
        message0: "%{BKY_SS_RASPBOT_IR_AVOID}",
        output: "Boolean",
        outputShape: Blockly.OUTPUT_SHAPE_ROUND,
        colour: "%{BKY_COLOR_OF_DETECTION}"
    }]);

    /*** Blockly Python **/
    Blockly.Python['ss_raspbot_ir_avoid'] = function (block) {
        var order = Blockly.Python.ORDER_ATOMIC;
        var code = Blockly.Python.format("IR_avoid()");
        return [code, order];
    };

    /*** Blokly Message **/
    Blockly.Msg.en["SS_RASPBOT_IR_AVOID"] = "infra-red avoiding";
    Blockly.Msg.zhhant["SS_RASPBOT_IR_AVOID"] = "紅外線避障";


} 
