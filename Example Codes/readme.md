原实验取自 V-Car 2-3



步骤：

1. 上传 myTestBlocks.js 积木附件（文件夹形式）
2. 将 myToolboxs.xml 内容贴到工具栏表单
3. 生成课程包


现象：

1. 工具栏动态增加了两个积木

行动：ss_vcar_go3
检测：ss_vcar_incurrenttask2

这两个积木可以正常在多语言下显示，生成自己的代码

2.

SS_BLOCK_VCAR_GO2 的中文名字被覆盖了，显示：前进2被修改了



