console.log('动态制作加载新的积木')

function ss_blockly_inject(Blockly) {
    console.log('开给 blockly 注入新积木属性')


    /*****************************************************************************************************************************************/
    /***************************************************** Begin of Injecting New Blocks *****************************************************/
    /*****************************************************************************************************************************************/


    /************************************************************************************************************************************/

    Blockly.defineBlocksWithJsonArray([{
        type: "ss_sstorm_go",
        message0: "%{BKY_SS_BLOCK_SSTORM_GO}",
        args0: [{
            type: "input_value",
            name: "LEFT",
            check: "Number"
        }, {
            type: "input_value",
            name: "RIGHT",
            check: "Number"
        }, {
            type: "input_value",
            name: "TIME",
            check: "Number"
        }],
        previousStatement: null,
        nextStatement: null,
        colour: "%{BKY_COLOR_OF_ACTION}"
    }]);

    /*** Blockly Python **/
    Blockly.Python['ss_sstorm_go'] = function (block) {
        var argument0 = Blockly.Python.valueToCode(block, 'LEFT', Blockly.Python.ORDER_ATOMIC) || 'None';
        var argument1 = Blockly.Python.valueToCode(block, 'RIGHT', Blockly.Python.ORDER_ATOMIC) || 'None';
        var argument2 = Blockly.Python.valueToCode(block, 'TIME', Blockly.Python.ORDER_ATOMIC) || 'None';
        var code = Blockly.Python.format("go({0}, {1}, {2})", argument0, argument1, argument2);
        return code + "\n";
    };

    /*** Blokly Message **/
    Blockly.Msg.en["SS_BLOCK_SSTORM_GO"] = "go %1, %2, %3 seconds";
    Blockly.Msg.zhhant["SS_BLOCK_SSTORM_GO"] = "前進 %1, %2, %3 秒";

    /************************************************************************************************************************************/

    Blockly.defineBlocksWithJsonArray([{
        type: "ss_sstorm_run",
        message0: "%{BKY_SS_BLOCK_SSTORM_RUN}",
        args0: [{
            type: "input_value",
            name: "LEFT",
            check: "Number"
        }, {
            type: "input_value",
            name: "RIGHT",
            check: "Number"
        }, {
            type: "input_value",
            name: "TIME",
            check: "Number"
        }],
        previousStatement: null,
        nextStatement: null,
        colour: "%{BKY_COLOR_OF_ACTION}"
    }]);

    /*** Blockly Python **/
    Blockly.Python['ss_sstorm_run'] = function (block) {
        var argument0 = Blockly.Python.valueToCode(block, 'LEFT', Blockly.Python.ORDER_ATOMIC) || 'None';
        var argument1 = Blockly.Python.valueToCode(block, 'RIGHT', Blockly.Python.ORDER_ATOMIC) || 'None';
        var argument2 = Blockly.Python.valueToCode(block, 'TIME', Blockly.Python.ORDER_ATOMIC) || 'None';
        var code = Blockly.Python.format("run({0}, {1}, {2})", argument0, argument1, argument2);
        return code + "\n";
    };

    /*** Blokly Message **/
    Blockly.Msg.en["SS_BLOCK_SSTORM_RUN"] = "run %1, %2, %3 seconds";
    Blockly.Msg.zhhant["SS_BLOCK_SSTORM_RUN"] = "持續前進 %1, %2, %3 秒";

    /************************************************************************************************************************************/

    Blockly.defineBlocksWithJsonArray([{
        type: "ss_sstorm_getcolor",
        message0: "%{BKY_SS_BLOCK_SSTORM_GETCOLOR}",
        output: "Boolean",
        outputShape: Blockly.OUTPUT_SHAPE_ROUND,
        colour: "%{BKY_COLOR_OF_DETECTION}"
    }]);

    /*** Blockly Python **/
    Blockly.Python['ss_sstorm_getcolor'] = function (block) {
        var order = Blockly.Python.ORDER_ATOMIC;
        var code = Blockly.Python.format("getColor()");
        return [code, order];
    };

    /*** Blokly Message **/
    Blockly.Msg.en["SS_BLOCK_SSTORM_GETCOLOR"] = "the detected color";
    Blockly.Msg.zhhant["SS_BLOCK_SSTORM_GETCOLOR"] = "檢測到的顏色";

    /************************************************************************************************************************************/

    Blockly.defineBlocksWithJsonArray([{
        type: "ss_sstorm_getdistance",
        message0: "%{BKY_SS_BLOCK_SSTORM_GETDISTANCE}",
        output: "Boolean",
        outputShape: Blockly.OUTPUT_SHAPE_ROUND,
        colour: "%{BKY_COLOR_OF_DETECTION}"
    }]);

    /*** Blockly Python **/
    Blockly.Python['ss_sstorm_getdistance'] = function (block) {
        var order = Blockly.Python.ORDER_ATOMIC;
        var code = Blockly.Python.format("getDistance()");
        return [code, order];
    };

    /*** Blokly Message **/
    Blockly.Msg.en["SS_BLOCK_SSTORM_GETDISTANCE"] = "distance";
    Blockly.Msg.zhhant["SS_BLOCK_SSTORM_GETDISTANCE"] = "檢測到的距離";

    /************************************************************************************************************************************/

    Blockly.defineBlocksWithJsonArray([{
        type: "ss_sstorm_print_var",
        message0: "%{BKY_SS_BLOCK_SSTORM_PRINT_VAR}",
        args0: [{
            type: "input_value",
            name: "TEXT"
        }],
        previousStatement: null,
        nextStatement: null,
        colour: "%{BKY_COLOR_OF_OUTPUT}"
    }]);

    /*** Blockly Python **/
    Blockly.Python['ss_sstorm_print_var'] = function (block) {
        var argument0 = Blockly.Python.valueToCode(block, "TEXT", Blockly.Python.ORDER_NONE) || "''";
        return "print(" + argument0 + ")\n"
    };

    /*** Blokly Message **/
    Blockly.Msg.en["SS_BLOCK_SSTORM_PRINT_VAR"] = "print %1";
    Blockly.Msg.zhhant["SS_BLOCK_SSTORM_PRINT_VAR"] = "打印 %1";

    /************************************************************************************************************************************/

    Blockly.defineBlocksWithJsonArray([{
        type: "ss_sstorm_speak",
        message0: "%{BKY_SS_BLOCK_SSTORM_SPEAK}",
        args0: [{
            type: "input_value",
            name: "MESSAGE"
        }],
        previousStatement: null,
        nextStatement: null,
        colour: "%{BKY_COLOR_OF_ACTION}"
    }]);

    /*** Blockly Python **/
    Blockly.Python['ss_sstorm_speak'] = function (block) {
        // speak('')
        var argument0 = Blockly.Python.valueToCode(block, 'MESSAGE', Blockly.Python.ORDER_ATOMIC) || 'None';
        var code = Blockly.Python.format("speak({0})", argument0);
        return code + "\n";
    };

    /*** Blokly Message **/
    Blockly.Msg.en["SS_BLOCK_SSTORM_SPEAK"] = "speak %1";
    Blockly.Msg.zhhant["SS_BLOCK_SSTORM_SPEAK"] = "講 %1";

    /************************************************************************************************************************************/

    Blockly.defineBlocksWithJsonArray([{
        type: "ss_sstorm_sleep",
        message0: "%{BKY_SS_BLOCK_SSTORM_SLEEP}",
        args0: [{
            type: "input_value",
            name: "NUM",
            check: "Number"
        }],
        previousStatement: null,
        nextStatement: null,
        colour: "%{BKY_COLOR_OF_ACTION}"
    }]);

    /*** Blockly Python **/
    Blockly.Python['ss_sstorm_sleep'] = function (block) {
        var argument0 = Blockly.Python.valueToCode(block, "NUM", Blockly.Python.ORDER_NONE) || 'None';
        return "sleep(" + argument0 + ")\n"
    };

    /*** Blokly Message **/
    Blockly.Msg.en["SS_BLOCK_SSTORM_SLEEP"] = "sleep %1 seconds";
    Blockly.Msg.zhhant["SS_BLOCK_SSTORM_SLEEP"] = "停 %1 秒";

    /************************************************************************************************************************************/

    Blockly.defineBlocksWithJsonArray([{
        type: "ss_sstorm_motor_run_time",
        message0: "%{BKY_SS_BLOCK_SSTORM_MOTOR_RUN_TIME}",
        args0: [{
            type: "field_dropdown",
            name: "MOTOR",
            options: [
                ["%{BKY_SS_BLOCK_SSTORM_DROPDOWN_MOTOR_A}", "motor_a"],
                ["%{BKY_SS_BLOCK_SSTORM_DROPDOWN_MOTOR_B}", "motor_b"],
                ["%{BKY_SS_BLOCK_SSTORM_DROPDOWN_MOTOR_C}", "motor_c"],
                ["%{BKY_SS_BLOCK_SSTORM_DROPDOWN_MOTOR_D}", "motor_d"]
            ]
        }, {
            type: "input_value",
            name: "SPEED",
            check: "Number"
        }, {
            type: "input_value",
            name: "TIME",
            check: "Number"
        }],
        previousStatement: null,
        nextStatement: null,
        colour: "%{BKY_COLOR_OF_ACTION}"
    }]);

    /*** Blockly Python **/
    Blockly.Python['ss_sstorm_motor_run_time'] = function (block) {
        var argument0 = block.getFieldValue("MOTOR");
        var argument1 = Blockly.Python.valueToCode(block, "SPEED", Blockly.Python.ORDER_ATOMIC) || "0";
        var argument2 = Blockly.Python.valueToCode(block, "TIME", Blockly.Python.ORDER_ATOMIC) || "0";
        return argument0 + ".run_time(" + argument1 + "," + argument2 + ")" + "\n";
    };

    /*** Blokly Message **/
    Blockly.Msg.en["SS_BLOCK_SSTORM_MOTOR_RUN_TIME"] = "move Motor %1 with a speed of %2 for %3 seconds";
    Blockly.Msg.zhhant["SS_BLOCK_SSTORM_MOTOR_RUN_TIME"] = "電機 %1 以 %2 的速度移動 %3 秒鐘";

    Blockly.Msg.en["SS_BLOCK_SSTORM_DROPDOWN_MOTOR_A"] = "A";
    Blockly.Msg.zhhant["SS_BLOCK_SSTORM_DROPDOWN_MOTOR_A"] = "A";

    Blockly.Msg.en["SS_BLOCK_SSTORM_DROPDOWN_MOTOR_B"] = "B";
    Blockly.Msg.zhhant["SS_BLOCK_SSTORM_DROPDOWN_MOTOR_B"] = "B";

    Blockly.Msg.en["SS_BLOCK_SSTORM_DROPDOWN_MOTOR_C"] = "C";
    Blockly.Msg.zhhant["SS_BLOCK_SSTORM_DROPDOWN_MOTOR_A"] = "C";

    Blockly.Msg.en["SS_BLOCK_SSTORM_DROPDOWN_MOTOR_D"] = "D";
    Blockly.Msg.zhhant["SS_BLOCK_SSTORM_DROPDOWN_MOTOR_B"] = "D";

    /*****************************************************************************************************************************************/
    /***************************************************** End of Injecting New Blocks *****************************************************/
    /*****************************************************************************************************************************************/
}