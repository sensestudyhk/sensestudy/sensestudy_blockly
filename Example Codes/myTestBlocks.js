console.log('动态制作加载新的积木')
function ss_blockly_inject(Blockly) {
    console.log('开给 blockly 注入新积木属性')

    // 积木样式定义
    Blockly.defineBlocksWithJsonArray([
        {
            "type": "ss_vcar_go3",
            "message0": "%{BKY_SS_BLOCK_VCAR_GO3}",
            "args0": [{
                "type": "input_value",
                "name": "LEFT",
                "check": "Number"
            }, {
                "type": "input_value",
                "name": "RIGHT",
                "check": "Number"
            }, {
                "type": "input_value",
                "name": "TIME",
                "check": "Number"
            }],
            "previousStatement": null,
            "nextStatement": null,
            "colour": "%{BKY_COLOR_OF_ACTION}"
        }, {
            "type": "ss_vcar_incurrenttask2",
            "message0": "%{BKY_SS_BLOCK_VCAR_INCURRENTTASK2}",
            "output": "Boolean",
            "outputShape": Blockly.OUTPUT_SHAPE_HEXAGONAL,
            "colour": "%{BKY_COLOR_OF_DETECTION}"
        },]);

    // 积木产生 python 代码
    Blockly.Python['ss_vcar_go3'] = function (block) {
        // go [left] [right] [time]
        var argument0 = Blockly.Python.valueToCode(block, 'LEFT', Blockly.Python.ORDER_ATOMIC) || '0';
        var argument1 = Blockly.Python.valueToCode(block, 'RIGHT', Blockly.Python.ORDER_ATOMIC) || '0';
        var argument2 = Blockly.Python.valueToCode(block, 'TIME', Blockly.Python.ORDER_ATOMIC) || '0';
        var code = Blockly.Python.format("go3({0}, {1}, {2})", argument0, argument1, argument2);
        return code + "\\n";
    };

    Blockly.Python['ss_vcar_incurrenttask2'] = function (block) {
        var order = Blockly.Python.ORDER_ATOMIC;
        var code = Blockly.Python.format("inCurrentTask2()");
        return [code, order];
    };

    // 积木文字的多语言设置
    Blockly.Msg.en["SS_BLOCK_VCAR_GO3"] = "go3(L%1, R%2, t%3)";
    Blockly.Msg.zhhans["SS_BLOCK_VCAR_GO3"] = "前进3(L%1, R%2, t%3)";
    Blockly.Msg.zhhant["SS_BLOCK_VCAR_GO3"] = "前進3(L%1, R%2, t%3)";
    Blockly.Msg.ja["SS_BLOCK_VCAR_GO3"] = "前へ進む3(L%1, R%2, t%3)";
    Blockly.Msg.zhhans["SS_BLOCK_VCAR_GO2"] = "go2被修改了(L%1, R%2, t%3)";
    Blockly.Msg.en["SS_BLOCK_VCAR_INCURRENTTASK2"] = "INCURRENTTASK2";
    Blockly.Msg.zhhans["SS_BLOCK_VCAR_INCURRENTTASK2"] = "当前任务2";
}