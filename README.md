# Some Existing JavaScripts and XML for Blockly at SenseStudy

## Catalog
### 1. animaldetection. 
- Extended blocks for Elementary AI, AnimalDetection.

### 2. Chapter7. 
- CUHK Chapter7, blocks for three experiments.

### 3. classification.
- classification robot on SenseStorm Workshop.

### 4. competition.
- Scheduled classification competition on SenseStudy

### 5. emoji.
- Elementary AI course imported from mainland. ***Make yourself a emoji in gif.***

### 6. Example Codes.
- Example only.

### 7. FaceComparison.
- Elementary AI course imported from mainland. ***Compare Face feature.***

### 8. IFTTT.
- not accomplished tool to bridge the gap between sensestorm and IFTTT.

### 9. imageCaption.
- Elementary AI course imported from mainland. ***Caption an image by understanding the scene***

### 10. Raspbot.
- Purchased raspberry pi car, control commands on SenseStudy.
