# Chapter7: AI Reasoning

## Experiment 1

### Step1: 
    Connect your SenseStorm
    連接您的SenseStorm

Click the "Connect Hardware" button on the top right corner of current page.
單擊當前頁面右上角的“連接硬件”按鈕。

### Step2:
    Learn about distance detection by Bounding Box and Motor Encoder Value
    了解有關通過邊界框和電機編碼器值進行距離檢測的信息

In this experiment, we are going to explore the awesome features of auto-driving with SenseStorm.
在本實驗中，我們將探索SenseStorm自動駕駛的強大功能。

There are six pre-trained models with two different detection methods to estimate the braking distance of SenseStorm.
有六個具有兩種不同檢測方法的預訓練模型，用於估計SenseStorm的製動距離。

Sample models by bounding box detection:
通過邊界框檢測的樣本模型：

``` python
# the brakng point of the SenseStorm will be 70-75 cm away from the traffic light
# SenseStorm的剎車點距離交通信號燈70-75厘米
Sample_BoundingBox_far() 

# the brakng point of the SenseStorm will be 40-45 cm away from the traffic light
# SenseStorm的剎車點距離交通信號燈40-45厘米
Sample_BoundingBox_mid()

# the brakng point of the SenseStorm will be 20-25 cm away from the traffic light
# SenseStorm的剎車點距離交通信號燈20-25厘米
Sample_BoundingBox_close() 
```
![Sample_Bounding_Box_Eng](src_img/Sample_bb_Eng.png)
![]
Sample models by encoder estimation:
通過編碼器估算的樣本模型：
```python
# the brakng point of the SenseStorm will be 70-75 cm away from the traffic light
# SenseStorm的剎車點距離交通信號燈70-75厘米
Sample_Encoder_far() 

# the brakng point of the SenseStorm will be 40-45 cm away from the traffic light
# SenseStorm的剎車點距離交通信號燈40-45厘米
Sample_Encoder_mid()

# the brakng point of the SenseStorm will be 20-25 cm away from the traffic light
# SenseStorm的剎車點距離交通信號燈20-25厘米
Sample_Encoder_close() 
```

### Step3: 