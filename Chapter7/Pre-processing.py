### -------------------------------------------------- code for Experiment 1 ----------------------------- ###
def Sample_BoundingBox_close():
   import sys
   sys.path.append('/home/pi/Desktop/car-simulator-python/Samples')
   import os
   os.chdir('/home/pi/Desktop/car-simulator-python/Samples')
   from Sample_BoundingBox_close import Sample_BoundingBox_close
   Sample_BoundingBox_close()

def Sample_BoundingBox_mid():
   import sys
   sys.path.append('/home/pi/Desktop/car-simulator-python/Samples')
   import os
   os.chdir('/home/pi/Desktop/car-simulator-python/Samples')
   from Sample_BoundingBox_mid import Sample_BoundingBox_mid
   Sample_BoundingBox_mid()

def Sample_BoundingBox_far():
   import sys
   sys.path.append('/home/pi/Desktop/car-simulator-python/Samples')
   import os
   os.chdir('/home/pi/Desktop/car-simulator-python/Samples')
   from Sample_BoundingBox_far import Sample_BoundingBox_far
   Sample_BoundingBox_far()

def Sample_Encoder_close():
   import sys
   sys.path.append('/home/pi/Desktop/car-simulator-python/Samples')
   import os
   os.chdir('/home/pi/Desktop/car-simulator-python/Samples')
   from Sample_Encoder_close import Sample_Encoder_close
   Sample_Encoder_close()

def Sample_Encoder_mid():
   import sys
   sys.path.append('/home/pi/Desktop/car-simulator-python/Samples')
   import os
   os.chdir('/home/pi/Desktop/car-simulator-python/Samples')
   from Sample_Encoder_mid import Sample_Encoder_mid
   Sample_Encoder_mid()

def Sample_Encoder_far():
   import sys
   sys.path.append('/home/pi/Desktop/car-simulator-python/Samples')
   import os
   os.chdir('/home/pi/Desktop/car-simulator-python/Samples')
   from Sample_Encoder_far import Sample_Encoder_far
   Sample_Encoder_far()

### ---------------------------------------- code for Experiment ------------------------------------- 
###
def Model1_BoundingBox():
   import sys
   sys.path.append('/home/pi/Desktop/car-simulator-python/Model1')
   import os
   os.chdir('/home/pi/Desktop/car-simulator-python/Model1')
   from Model1_BoundingBox import Model1_BoundingBox
   Model1_BoundingBox()

def Model1_Encoder():
   import sys
   sys.path.append('/home/pi/Desktop/car-simulator-python/Model1')
   import os
   os.chdir('/home/pi/Desktop/car-simulator-python/Model1')
   from Model1_Encoder import Model1_Encoder
   Model1_Encoder()

def Model1_convert(user_ID):
   import sys
   sys.path.append('/home/pi/Desktop/car-simulator-python/Model1')
   import os
   os.chdir('/home/pi/Desktop/car-simulator-python/Model1')
   from Model1_conversion import Model1_conversion 
   Model1_conversion (user_ID)

def Model1_replace(user_ID):
   import sys
   sys.path.append('/home/pi/Desktop/car-simulator-python/Model1')
   import os
   os.chdir('/home/pi/Desktop/car-simulator-python/Model1')
   from Model1_replace import Model1_replace
   Model1_replace (user_ID)

### -------------------------------- Code for Experiment 3 -------------------------- ###

def Model2_BoundingBox():
   import sys
   sys.path.append('/home/pi/Desktop/car-simulator-python/Model2')
   import os
   os.chdir('/home/pi/Desktop/car-simulator-python/Model2')
   from Model2_BoundingBox import Model2_BoundingBox
   Model2_BoundingBox()

def Model2_Encoder():
   import sys
   sys.path.append('/home/pi/Desktop/car-simulator-python/Model2')
   import os
   os.chdir('/home/pi/Desktop/car-simulator-python/Model2')
   from Model2_Encoder import Model2_Encoder
   Model2_Encoder()

def Model2_convert():
   import sys
   sys.path.append('/home/pi/Desktop/car-simulator-python/Model2')
   import os
   os.chdir('/home/pi/Desktop/car-simulator-python/Model2')
   from Model2_conversion import Model2_conversion 
   Model2_conversion()

def Model2_replace(user_ID):
   import sys
   sys.path.append('/home/pi/Desktop/car-simulator-python/Model2')
   import os
   os.chdir('/home/pi/Desktop/car-simulator-python/Model2')
   from Model2_replace import Model2_replace
   Model2_replace (user_ID)

def Model2_DataCollection():
   import sys
   sys.path.append('/home/pi/Desktop/car-simulator-python/Model2')
   import os
   os.chdir('/home/pi/Desktop/car-simulator-python/Model2')
   from Model2_DataCollection import Model2_DataCollection
   Model2_DataCollection()
