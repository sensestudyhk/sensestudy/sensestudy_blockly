console.log('动态制作加载新的积木')
function ss_blockly_inject(Blockly) {
    console.log('开给 blockly 注入新积木属性')
    
//****************************************** sample_boundingbox_close ******************************************/ 
    // Define the Style of designed Blocks
    Blockly.defineBlocksWithJsonArray([
        {
            // sample model with boundingbox detection methods
            type: "ss_sample_boundingbox_close", 
            message0: "%{BKY_SS_BLOCK_SAMPLE_BOUNDINGBOX_CLOSE}", 
            previousStatement: null,
            nextStatement: null,
            colour: "%{BKY_COLOR_OF_ACTION}" 
        }   
    ]);
    
    // Inject the python function
    Blockly.Python['ss_sample_boundingbox_close'] = function (block) {
        var code = Blockly.Python.format("Sample_BoundingBox_close()");  
        return code + "\n"
    };

    // displayed content under multiple languages
    Blockly.Msg.en["SS_BLOCK_SAMPLE_BOUNDINGBOX_CLOSE"] = "Sample BoundingBox: close to the traffic Light";
    Blockly.Msg.zhhant["SS_BLOCK_SAMPLE_BOUNDINGBOX_CLOSE"] = "近距離示例：邊框相對算法";


//****************************************** sample_encoder_close ******************************************/ 
    Blockly.defineBlocksWithJsonArray([
        {
            // sample model with motor encoding methods
            type: "ss_sample_encoder_close", 
            message0: "%{BKY_SS_BLOCK_SAMPLE_ENCODER_CLOSE}", 
            previousStatement: null,
            nextStatement: null,
            colour: "%{BKY_COLOR_OF_ACTION}" 
        }
    ]);

    Blockly.Python['ss_sample_encoder_close'] = function (block) {
        var code = Blockly.Python.format("Sample_Encoder_close()");  
        return code + "\n"
    };

    Blockly.Msg.en["SS_BLOCK_SAMPLE_ENCODER_CLOSE"] = "Sample MotorEncoder: close to the traffic Light"; 
    Blockly.Msg.zhhant["SS_BLOCK_SAMPLE_ENCODER_CLOSE"] = "近距離示例：電機解碼算法"; 

//****************************************** sample_boundingbox_mid ******************************************/ 
    Blockly.defineBlocksWithJsonArray([
        {
            // sample model with boundingbox detection methods
            type: "ss_sample_boundingbox_mid", 
            message0: "%{BKY_SS_BLOCK_SAMPLE_BOUNDINGBOX_MID}", 
            previousStatement: null,
            nextStatement: null,
            colour: "%{BKY_COLOR_OF_ACTION}" 
        }
    ]);

    Blockly.Python['ss_sample_boundingbox_mid'] = function (block) {
        var code = Blockly.Python.format("Sample_BoundingBox_mid()");  
        return code + "\n"
    };

    Blockly.Msg.en["SS_BLOCK_SAMPLE_BOUNDINGBOX_MID"] = "Sample BoundingBox: middle of the way";
    Blockly.Msg.zhhant["SS_BLOCK_SAMPLE_BOUNDINGBOX_MID"] = "中段距離示例：邊框相對算法"; 

//****************************************** sample_encoder_mid ******************************************/ 
    Blockly.defineBlocksWithJsonArray([
        {
            // sample model with motor encoding methods
            type: "ss_sample_encoder_mid", 
            message0: "%{BKY_SS_BLOCK_SAMPLE_ENCODER_MID}", 
            previousStatement: null,
            nextStatement: null,
            colour: "%{BKY_COLOR_OF_ACTION}" 
        }
    ]);

    Blockly.Python['ss_sample_encoder_mid'] = function (block) {
        var code = Blockly.Python.format("Sample_Encoder_mid()");  
        return code + "\n"
    };

    Blockly.Msg.en["SS_BLOCK_SAMPLE_ENCODER_MID"] = "Sample MotorEncoder: middle of the way"; 
    Blockly.Msg.zhhant["SS_BLOCK_SAMPLE_ENCODER_MID"] = "中段距離示例示例：電機解碼算法";

//****************************************** sample_boundingbox_far ******************************************/ 
    Blockly.defineBlocksWithJsonArray([
        {
            // sample model with boundingbox detection methods
            type: "ss_sample_boundingbox_far", 
            message0: "%{BKY_SS_BLOCK_SAMPLE_BOUNDINGBOX_FAR}", 
            previousStatement: null,
            nextStatement: null,
            colour: "%{BKY_COLOR_OF_ACTION}" 
        }
    ]);

    Blockly.Python['ss_sample_boundingbox_far'] = function (block) {
        var code = Blockly.Python.format("Sample_BoundingBox_far()");  
        return code + "\n"
    };

    Blockly.Msg.en["SS_BLOCK_SAMPLE_BOUNDINGBOX_FAR"] = "Sample BoundingBox: far from the traffic light";
    Blockly.Msg.zhhant["SS_BLOCK_SAMPLE_BOUNDINGBOX_FAR"] = "遠距離示例：邊框相對算法"; 

//****************************************** sample_encoder_far ******************************************/ 
    Blockly.defineBlocksWithJsonArray([
        {
            // sample model with motor encoding methods
            type: "ss_sample_encoder_far", 
            message0: "%{BKY_SS_BLOCK_SAMPLE_ENCODER_FAR}", 
            previousStatement: null,
            nextStatement: null,
            colour: "%{BKY_COLOR_OF_ACTION}" 
        }
    ]);

    Blockly.Python['ss_sample_encoder_far'] = function (block) {
        var code = Blockly.Python.format("Sample_Encoder_far()");  
        return code + "\n"
    };

    Blockly.Msg.en["SS_BLOCK_SAMPLE_ENCODER_FAR"] = "Sample MotorEncoder: far from the traffic light"; 
    Blockly.Msg.zhhant["SS_BLOCK_SAMPLE_ENCODER_FAR"] = "遠距離示例：電機解碼算法"; 

//****************************************** model1_boundingbox ******************************************/ 
    Blockly.defineBlocksWithJsonArray([
        {
            // web-genarated model1 with boundingbox detection methods
            type: "ss_model1_boundingbox", 
            message0: "%{BKY_SS_BLOCK_MODEL1_BOUNDINGBOX}", 
            previousStatement: null,
            nextStatement: null,
            colour: "%{BKY_COLOR_OF_ACTION}" 
        }
    ]);

    Blockly.Python['ss_model1_boundingbox'] = function (block) { 
        var code = Blockly.Python.format("Model1_BoundingBox()"); 
        return code + "\n"
    };

    Blockly.Msg.en["SS_BLOCK_MODEL1_BOUNDINGBOX"] = "Model1: Bounding Box"; 
    Blockly.Msg.zhhant["SS_BLOCK_MODEL1_BOUNDINGBOX"] = "模型一：邊框相對算法";

//****************************************** model1_encoder ******************************************/ 
    Blockly.defineBlocksWithJsonArray([
        {
            // web-genarated model1 with motor encoding methods
            type: "ss_model1_encoder", 
            message0: "%{BKY_SS_BLOCK_MODEL1_ENCODER}", 
            previousStatement: null,
            nextStatement: null,
            colour: "%{BKY_COLOR_OF_ACTION}" 
        }
    ]);

    Blockly.Python['ss_model1_encoder'] = function (block) { 
        var code = Blockly.Python.format("Model1_Encoder()"); 
        return code + "\n"
    };

    Blockly.Msg.en["SS_BLOCK_MODEL1_ENCODER"] = "Model1：MotorEncoder"; 
    Blockly.Msg.zhhant["SS_BLOCK_MODEL1_ENCODER"] = "模型一：電機解碼算法"; 

//****************************************** model1_convert ******************************************/
    Blockly.defineBlocksWithJsonArray([
        {
            // conversion method to convert the model from tfjs to pb
            type: "ss_model1_convert",  
            message0: "%{BKY_SS_BLOCK_MODEL1_CONVERT}",
            args0: [{
                type: "input_value",
                name: "MESSAGE",
                check: "String"
            }],
            previousStatement: null,
            nextStatement: null,
            colour: "%{BKY_COLOR_OF_ACTION}"
        }
    ]);

    Blockly.Python['ss_model1_convert'] = function (block) { 
        var argument0 = Blockly.Python.valueToCode(block, 'MESSAGE', Blockly.Python.ORDER_ATOMIC) || 'None';
        var code = Blockly.Python.format("Model1_convert({0})", argument0);
        return code + "\n"
    };

    Blockly.Msg.en["SS_BLOCK_MODEL1_CONVERT"] = "Model1：Conversion %1"; 
    Blockly.Msg.zhhant["SS_BLOCK_MODEL1_CONVERT"] = "模型一：模型類別轉換 %1";

//****************************************** model1_replace ******************************************/
    Blockly.defineBlocksWithJsonArray([
        {
            type: "ss_model1_replace", 
            message0: "%{BKY_SS_BLOCK_MODEL1_REPLACE}",
            args0: [{
                type: "input_value",
                name: "MESSAGE",
                check: "String"
            }],
            previousStatement: null,
            nextStatement: null,
            colour: "%{BKY_COLOR_OF_ACTION}" 
        }
    ]);

    Blockly.Python['ss_model1_replace'] = function (block) { 
        var argument0 = Blockly.Python.valueToCode(block, 'MESSAGE', Blockly.Python.ORDER_ATOMIC) || 'None';
        var code = Blockly.Python.format("Model1_replace({0})", argument0); 
        return code + "\n"
    };

    Blockly.Msg.en["SS_BLOCK_MODEL1_REPLACE"] = "Model1：Replace the model by %1"; 
    Blockly.Msg.zhhant["SS_BLOCK_MODEL1_REPLACE"] = "模型一：使用 %1 替换原有模型";

//****************************************** model2_boundingbox ******************************************/
    Blockly.defineBlocksWithJsonArray([
        {
            // web-genarated model2 with boundingbox detection methods
            type: "ss_model2_boundingbox", 
            message0: "%{BKY_SS_BLOCK_MODEL2_BOUNDINGBOX}", 
            previousStatement: null,
            nextStatement: null,
            colour: "%{BKY_COLOR_OF_ACTION}" 
        }
    ]);

    Blockly.Python['ss_model2_boundingbox'] = function (block) { 
        var code = Blockly.Python.format("Model2_BoundingBox()"); 
        return code + "\n"
    };

    Blockly.Msg.en["SS_BLOCK_MODEL2_BOUNDINGBOX"] = "Model2: Bounding Box"; 
    Blockly.Msg.zhhant["SS_BLOCK_MODEL2_BOUNDINGBOX"] = "模型二：邊框相對算法";

//****************************************** model2_encoder ******************************************/
    Blockly.defineBlocksWithJsonArray([
        {
            // web-genarated model2 with motor encoding methods
            type: "ss_model2_encoder", 
            message0: "%{BKY_SS_BLOCK_MODEL2_ENCODER}", 
            previousStatement: null,
            nextStatement: null,
            colour: "%{BKY_COLOR_OF_ACTION}" 
        }
    ]);

    Blockly.Python['ss_model2_encoder'] = function (block) { 
        var code = Blockly.Python.format("Model2_Encoder()"); 
        return code + "\n"
    };

    Blockly.Msg.en["SS_BLOCK_MODEL2_ENCODER"] = "Model2：MotorEncoder"; 
    Blockly.Msg.zhhant["SS_BLOCK_MODEL2_ENCODER"] = "模型二：電機解碼算法"; 

//****************************************** model2_convert ******************************************/
    Blockly.defineBlocksWithJsonArray([
        {
            // conversion method to convert the model from tfjs to pb
            type: "ss_model2_convert", 
            message0: "%{BKY_SS_BLOCK_MODEL2_CONVERT}",
            args0: [{
                type: "input_value",
                name: "MESSAGE",
                check: "String"
            }],
            previousStatement: null,
            nextStatement: null,
            colour: "%{BKY_COLOR_OF_ACTION}" 
        }
    ]);

    Blockly.Python['ss_model2_convert'] = function (block) { 
        var argument0 = Blockly.Python.valueToCode(block, 'MESSAGE', Blockly.Python.ORDER_ATOMIC) || 'None';
        var code = Blockly.Python.format("Model2_convert({0})", argument0); 
        return code + "\n"
    };

    Blockly.Msg.en["SS_BLOCK_MODEL2_CONVERT"] = "Model2：Conversion %1"; 
    Blockly.Msg.zhhant["SS_BLOCK_MODEL2_CONVERT"] = "模型二：模型類別轉換 %1";

//****************************************** model2_datacollection ******************************************/
    Blockly.defineBlocksWithJsonArray([
        {
            type: "ss_model2_datacollection",
            message0: "%{BKY_SS_BLOCK_MODEL2_DATACOLLECTION}",
            previousStatement: null,
            nextStatement: null,
            colour: "%{BKY_COLOR_OF_ACTION}"
        }
    ]);

    Blockly.Python['ss_model2_datacollection'] = function (block) { 
        var code = Blockly.Python.format("Model2_DataCollection()"); 
        return code + "\n"
    };

    Blockly.Msg.en["SS_BLOCK_MODEL2_DATACOLLECTION"] = "Model2：DataCollection"; 
    Blockly.Msg.zhhant["SS_BLOCK_MODEL2_DATACOLLECTION"] = "模型二：數據收集";

//****************************************** model2_replace ******************************************/
    Blockly.defineBlocksWithJsonArray([
        {
            // conversion method to convert the model from tfjs to pb
            type: "ss_model2_replace", 
            message0: "%{BKY_SS_BLOCK_MODEL2_REPLACE}",
            args0: [{
                type: "input_value",
                name: "MESSAGE",
                check: "String"
            }],
            previousStatement: null,
            nextStatement: null,
            colour: "%{BKY_COLOR_OF_ACTION}" 
        }
    ]);

    Blockly.Python['ss_model2_replace'] = function (block) { 
        var argument0 = Blockly.Python.valueToCode(block, 'MESSAGE', Blockly.Python.ORDER_ATOMIC) || 'None';
        var code = Blockly.Python.format("Model2_replace({0})", argument0); 
        return code + "\n"
    };

    Blockly.Msg.en["SS_BLOCK_MODEL2_REPLACE"] = "Model2：Replace the model by %1"; 
    Blockly.Msg.zhhant["SS_BLOCK_MODEL2_REPLACE"] = "模型二：使用 %1 替换原有模型";

} 
