from setuptools import find_packages, setup
import os

def package_files(directory):
    paths = []
    for (path, directories, filenames) in os.walk(directory):
        for filename in filenames:
            paths.append(os.path.join('..', path, filename))
    return paths

extra_files = package_files('.')
print(extra_files)

setup(
    name='testfile',
    version='0.0.1',
    description='testfile',
    keywords='testfile',
    packages=find_packages(),
    include_package_data=True,
    package_data={'': extra_files},
)

# run with
# python setup.py bdist_wheel