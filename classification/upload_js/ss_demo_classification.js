console.log('动态制作加载新的积木')
function ss_blockly_inject(Blockly) {
    console.log('开给 blockly 注入新积木属性')

/************************************** extract_feature(imgs) ********************************/
    Blockly.defineBlocksWithJsonArray([
        {
            type: "ss_demo_extract_feature", 
            message0: "%{BKY_SS_BLOCK_DEMO_EXTRACT_FEATURE}",
            args0: [{
                type: "input_value",
                name: "imgs"
            }],
            output: "Boolean",
            outputShape: Blockly.OUTPUT_SHAPE_ROUND,
            colour: "%{BKY_COLOR_OF_DETECTION}"
        }
    ]);

    Blockly.Python['ss_demo_extract_feature'] = function (block) {
        var order = Blockly.Python.ORDER_ATOMIC;
        var argument0 = Blockly.Python.valueToCode(block, 'imgs', Blockly.Python.ORDER_ATOMIC) || 'None';
        var code = Blockly.Python.format("extract_feature({0})", argument0);
        return [code, order];
    };

    Blockly.Msg.en["SS_BLOCK_DEMO_EXTRACT_FEATURE"] = "extracted features from %1";
    Blockly.Msg.zhhant["SS_BLOCK_DEMO_EXTRACT_FEATURE"] = "从 %1 提取的特徵";

/************************************** LinearClassifier() ********************************/
    Blockly.defineBlocksWithJsonArray([
        {
            "type": "ss_demo_get_classifier", // origianl = ss_prim3_get_classifier
            "message0": "%{BKY_SS_BLOCK_DEMO_GET_CLASSIFIER}",
            "output": "Boolean",
            "outputShape": Blockly.OUTPUT_SHAPE_ROUND,
            "colour": "%{BKY_COLOR_OF_DETECTION}"
        }
    ]);
    
    Blockly.Python['ss_demo_get_classifier'] = function (block) {
        var order = Blockly.Python.ORDER_ATOMIC;
        var code = Blockly.Python.format("LinearClassifier()");
        return [code, order];
    };

    Blockly.Msg.en["SS_BLOCK_DEMO_GET_CLASSIFIER"] = "get classification model";
    Blockly.Msg.zhhant["SS_BLOCK_DEMO_GET_CLASSIFIER"] = "獲取分類模型";


/******************************** load folder via url **********************************/ 
    Blockly.defineBlocksWithJsonArray([
        {
            type: "ss_load_folder_url",
            message0: "%{BKY_SS_BLOCK_LOAD_FOLDER_URL}",
            args0:[{
                type: "input_value",
                name: "URL",
                check: "String"
            }],
            output: "Boolean",
            outputShape: Blockly.OUTPUT_SHAPE_ROUND,
            colour: "%{BKY_COLOR_OF_DETECTION}"
        }
    ]);

    Blockly.Python['ss_load_folder_url'] = function(block){
        var order = Blockly.Python.ORDER_ATOMIC;
        var argument0 = Blockly.Python.valueToCode(block, 'URL', Blockly.Python.ORDER_ATOMIC) || 'None';
        var code = Blockly.Python.format("load_folder_url({0})", argument0)
        return [code, order];
    };

    Blockly.Msg.en["SS_BLOCK_LOAD_FOLDER_URL"] = "load images from folder with url %1"
    Blockly.Msg.zhhant["SS_BLOCK_LOAD_FOLDER_URL"] = "從文件夾 %1 加載照片"


/***************************** add label**********************************************/
Blockly.defineBlocksWithJsonArray([
    {
        type: "ss_demo_add_label",
        message0: "%{BKY_SS_BLOCK_ADD_LABEL}",
        args0:[{
            type: "input_value",
            name: "NAME",
            check: "String"
        },{
            type: "input_value",
            name: "FOLDER",
            check: "String"
        }],
        output: "Boolean",
        outputShape: Blockly.OUTPUT_SHAPE_ROUND,
        colour: "%{BKY_COLOR_OF_DETECTION}"
    }
]);

Blockly.Python['ss_demo_add_label'] = function(block){
    var order = Blockly.Python.ORDER_ATOMIC;
    var argument0 = Blockly.Python.valueToCode(block, 'NAME', Blockly.Python.ORDER_ATOMIC) || None;
    var argument1 = Blockly.Python.valueToCode(block, 'FOLDER', Blockly.Python.ORDER_ATOMIC) || None;
    var code = Blockly.Python.format("add_label(" + argument0 + "," + argument1 + ")")
    return [code, order];
};

Blockly.Msg.en["SS_BLOCK_ADD_LABEL"] = "add labels with name of %1 to images on folder %2"
Blockly.Msg.zhhant["SS_BLOCK_ADD_LABEL"] = "將名稱為 %1 的標籤添加到文件夾 %2 的圖像上"

/***************************** test_live ********************************************/
Blockly.defineBlocksWithJsonArray([
    {
        type: "ss_demo_test_live",  
        message0: "%{BKY_SS_BLOCK_DEMO_TEST_LIVE}",
        args0: [{
            type: "input_value",
            name: "HANDLE"
        }],
        previousStatement: null,
        nextStatement: null,
        colour: "%{BKY_COLOR_OF_ACTION}"
    }
]);

Blockly.Python['ss_demo_test_live'] = function (block) { 
    var argument0 = Blockly.Python.valueToCode(block, 'HANDLE', Blockly.Python.ORDER_ATOMIC) || 'None';
    var code = Blockly.Python.format("test_live({0})", argument0);
    return code + "\n"
};

Blockly.Msg.en["SS_BLOCK_DEMO_TEST_LIVE"] = "Test objects in live video by %1"; 
Blockly.Msg.zhhant["SS_BLOCK_DEMO_TEST_LIVE"] = "通过 %1 測試實時視頻中的測試對象";

/************************************** test_images ********************************************/
Blockly.defineBlocksWithJsonArray([
    {
        type: "ss_demo_test_images",  
        message0: "%{BKY_SS_BLOCK_DEMO_TEST_IMAGES}",
        args0: [
          {
            type: "input_value",
            name: "HANDLE"
        },{
            type: "input_value",
            name: "IMGS"
        }],
        previousStatement: null,
        nextStatement: null,
        colour: "%{BKY_COLOR_OF_ACTION}"
    }
]);

Blockly.Python['ss_demo_test_images'] = function (block) { 
    var argument0 = Blockly.Python.valueToCode(block, 'HANDLE', Blockly.Python.ORDER_ATOMIC) || 'None';
    var argument1 = Blockly.Python.valueToCode(block, 'IMGS', Blockly.Python.ORDER_ATOMIC) || 'None';
    var code = "test_images(" + argument0 + "," + argument1 + ")";
    return code + "\n"
};

Blockly.Msg.en["SS_BLOCK_DEMO_TEST_IMAGES"] = "Load images from %2 and test them via %1"; 
Blockly.Msg.zhhant["SS_BLOCK_DEMO_TEST_IMAGES"] = "從 %2 加載圖像並用 %1 測試它們";

/******************************** filemove ****************************************************************/
Blockly.defineBlocksWithJsonArray([
    {
        type: "ss_demo_move_file",
        message0: "%{BKY_SS_BLOCK_DEMO_MOVE_FILE}",
        args0: [{
            type: "input_value",
            name: "NAME"
        }],
        previousStatement: null,
        nextStatement: null,
        colour: "%{BKY_COLOR_OF_ACTION}"
    }
]);

Blockly.Python['ss_demo_move_file'] = function (block) { 
    var argument0 = Blockly.Python.valueToCode(block, 'NAME', Blockly.Python.ORDER_ATOMIC) || 'None';
    var code = Blockly.Python.format("filemove({0})",argument0);
    return code + "\n"
};

Blockly.Msg.en["SS_BLOCK_DEMO_MOVE_FILE"] = "Move folder %1 to the target place"; 
Blockly.Msg.zhhant["SS_BLOCK_DEMO_MOVE_FILE"] = "將文件夾 %1 移動到目標位置";

/******************************************show********************************************************** */
Blockly.defineBlocksWithJsonArray([
    {
        type: "ss_demo_show", 
        message0: "%{BKY_SS_BLOCK_DEMO_SHOW}",
        args0: [{
            type: "input_value",
            name: "imgs"
        }],
        previousStatement: null,
        nextStatement: null,
        colour: "%{BKY_COLOR_OF_ACTION}"
    }
]);

Blockly.Python['ss_demo_show'] = function (block) {
    var order = Blockly.Python.ORDER_ATOMIC;
    var argument0 = Blockly.Python.valueToCode(block, 'imgs', Blockly.Python.ORDER_ATOMIC) || 'None';
    var code = Blockly.Python.format("show_image({0})", argument0);
    return code + "\n"
};

Blockly.Msg.en["SS_BLOCK_DEMO_SHOW"] = "show image %1";
Blockly.Msg.zhhant["SS_BLOCK_DEMO_SHOW"] = "顯示圖像 %1";

/****************************************** clear_ ********************************************************* */
Blockly.defineBlocksWithJsonArray([
    {
        type: "ss_demo_clear_all_data", 
        message0: "%{BKY_SS_BLOCK_DEMO_CLEAR_ALL_DATA}",
        previousStatement: null,
        nextStatement: null,
        colour: "%{BKY_COLOR_OF_ACTION}"
    }
]);

Blockly.Python['ss_demo_clear_all_data'] = function (block) {
    var order = Blockly.Python.ORDER_ATOMIC;
    var code = Blockly.Python.format("remove_all()");
    return code + "\n"
};

Blockly.Msg.en["SS_BLOCK_DEMO_CLEAR_ALL_DATA"] = "Clear all image data inside SenseStorm";
Blockly.Msg.zhhant["SS_BLOCK_DEMO_CLEAR_ALL_DATA"] = "清除 SenseStorm 中的所有圖像數據";

/****************************************** remove specific folder***************************************************** */
Blockly.defineBlocksWithJsonArray([
    {
        type: "ss_demo_folder_remove", 
        message0: "%{BKY_SS_BLOCK_DEMO_FOLDER_REMOVE}",
        args0: [{
            type: "input_value",
            name: "Folder"
        }],
        previousStatement: null,
        nextStatement: null,
        colour: "%{BKY_COLOR_OF_ACTION}"
    }
]);

Blockly.Python['ss_demo_folder_remove'] = function (block) {
    var order = Blockly.Python.ORDER_ATOMIC;
    var argument0 = Blockly.Python.valueToCode(block, 'Folder', Blockly.Python.ORDER_ATOMIC) || 'None';
    var code = Blockly.Python.format("file_remove({0})", argument0);
    return code + "\n"
};

Blockly.Msg.en["SS_BLOCK_DEMO_FOLDER_REMOVE"] = "Remove folder %1";
Blockly.Msg.zhhant["SS_BLOCK_DEMO_FOLDER_REMOVE"] = "刪除文件夾 %1";
}
