console.log('动态制作加载新的积木')
function ss_blockly_inject(Blockly) {
    console.log('开给 blockly 注入新积木属性')

    // 积木样式定义
    Blockly.defineBlocksWithJsonArray([
        {
            "type": "ss_get_net_handle", // origianl = ss_prim3_get_net_handle
            "message0": "%{BKY_SS_BLOCK_GET_NET_HANDLE}",
            "output": "Boolean",
            "outputShape": Blockly.OUTPUT_SHAPE_ROUND,
            "colour": "%{BKY_COLOR_OF_DETECTION}"
        },

        {
            "type": "ss_get_classifier", // origianl = ss_prim3_get_classifier
            "message0": "%{BKY_SS_BLOCK_GET_CLASSIFIER}",
            "output": "Boolean",
            "outputShape": Blockly.OUTPUT_SHAPE_ROUND,
            "colour": "%{BKY_COLOR_OF_DETECTION}"
        }
    ]);

    // 积木产生 python 代码
    Blockly.Python['ss_get_net_handle'] = function (block) {
        var order = Blockly.Python.ORDER_ATOMIC;
        var code = Blockly.Python.format("get_net_handle()");
        return [code, order];
    };

    Blockly.Python['ss_get_classifier'] = function (block) {
        var order = Blockly.Python.ORDER_ATOMIC;
        var code = Blockly.Python.format("get_classifier()");
        return [code, order];
    };

    // 积木文字的多语言设置
    Blockly.Msg.en["SS_BLOCK_GET_NET_HANDLE"] = "trained feature extraction model";
    Blockly.Msg.zhhant["SS_BLOCK_GET_NET_HANDLE"] = "已訓練的圖片特徵提取模型";

    Blockly.Msg.en["SS_BLOCK_GET_CLASSIFIER"] = "image classification model";
    Blockly.Msg.zhhant["SS_BLOCK_GET_CLASSIFIER"] = "圖片分類模型";


/******************************** load folder via url **********************************/ 
    Blockly.defineBlocksWithJsonArray([
        {
            type: "ss_load_folder_url",
            message0: "%{BKY_SS_BLOCK_LOAD_FOLDER_URL}",
            args0:[{
                type: "input_value",
                name: "url",
                check: "String"
            }],
            output: "Boolean",
            "outputShape": Blockly.OUTPUT_SHAPE_ROUND,
            colour: "%{BKY_COLOR_OF_DETECTION}"
        }
    ]);

    Blockly.Python['ss_load_folder_url'] = function(block){
        var order = Blockly.Python.ORDER_ATOMIC;
        var argument0 = Blockly.Python.valueToCode(block, 'url', Blockly.Python.ORDER_ATOMIC) || "None";
        var code = Blockly.Python.format("load_folder_url({0})", argument0)
        return [code, order];
    };

    Blockly.Msg.en["SS_BLOCK_LOAD_FOLDER_URL"] = "load multiple images from folder URL %1"
    Blockly.Msg.zhhant["SS_BLOCK_LOAD_FOLDER_URL"] = "從路徑為 %1 的文件夾加載多張圖片"
}