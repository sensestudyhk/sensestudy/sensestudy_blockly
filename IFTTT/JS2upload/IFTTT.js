console.log('动态制作加载新的积木')
function ss_blockly_inject(Blockly) {
    console.log('开给 blockly 注入新积木属性')

    // Define the Style of designed Blocks
    Blockly.defineBlocksWithJsonArray([
        {
            // sample model with boundingbox detection methods
            "type": "ss_color_detection", 
            "message0": "%{BKY_SS_BLOCK_COLOR_DETECTION}", 
            "previousStatement": null,
            "nextStatement": null,
            "colour": "%{BKY_COLOR_OF_ACTION}" 
        }
    ]);

    // assign python code to corresponding blocks
    Blockly.Python['ss_color_detection'] = function (block) {
        var code = Blockly.Python.format("Color_detection()");  
        return code + "\n"
    };
    // displayed content under multiple languages
    Blockly.Msg.en["SS_BLOCK_COLOR_DETECTION"] = "Detect the Red Color";
    Blockly.Msg.zhhant["SS_BLOCK_COLOR_DETECTION"] = "檢測紅色"; 
} 