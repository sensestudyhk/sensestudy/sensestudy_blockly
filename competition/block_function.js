console.log('动态制作加载新的积木')
function ss_blockly_inject(Blockly){
    console.log('开给 blockly 注入新积木属性')

    //****************************** detect_face *******************************//
    Blockly.defineBlocksWithJsonArray([
        {
            type: "ss_detect_face", 
            message0: "%{BKY_SS_DETECT_FACE}",
            args0: [{
                type: "input_value",
                name: "FRAME",
            }],
            output:"Boolean",
            outputShape: Blockly.OUTPUT_SHAPE_ROUND,
            colour: "%{BKY_COLOR_OF_DETECTION}" 
        }
    ]);

    Blockly.Python['ss_detect_face'] = function (block) { 
        var argument0 = Blockly.Python.valueToCode(block, 'FRAME', Blockly.Python.ORDER_ATOMIC) || 'None';
        var order = Blockly.Python.ORDER_ATOMIC;
        var code = Blockly.Python.format("detect_face({0})", argument0); 
        return [code, order]
    };

    Blockly.Msg.en["SS_DETECT_FACE"] = "Face location for frame %1"; 
    Blockly.Msg.zhhant["SS_DETECT_FACE"] = "图像 %1 的面部位置";

    //******************************** threading ******************************//
    Blockly.defineBlocksWithJsonArray([
        {
            type: "ss_thread",
            message0: "%{BKY_SS_THREAD}",
            args0: [
            {
                type: "input_value",
                name: "target",
            }],
            output: "Boolean",
            outputShape: Blockly.OUTPUT_SHAPE_ROUND,
            colour: "%{BKY_COLOR_OF_ACTION"
        }
    ]);

    Blockly.Python['ss_thread'] = function (block) {
        var argument0 = Blockly.Python.valueToCode(block, 'target', Blockly.Python.ORDER_ATOMIC) || 'None'0
        var order = Blockly.Python.ORDER_ATOMIC;
        var code = Blockly.Python.format("Thread(target={0}, args=(), daemon=True)", argument0); 
        return [code, order]
    };

    Blockly.Meg.en["SS_THREAD"] = "Set Thread %1"
    Blockly.Msg.zhhant["SS_THREAD"] = "设置新的线程 %1"
}
